#+TITLE: timu-caribbean-vim
#+AUTHOR: Aimé Bertrand
#+DATE: [2022-11-16 Wed]
#+LANGUAGE: en
#+OPTIONS: d:t toc:nil num:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://macowners.club/css/gtd.css" />
#+KEYWORDS: theme caribbean vim
#+STARTUP: indent content

Color theme with cyan as a dominant color.

* Support

- [[https://github.com/vim/vim][Vim]]
- [[https://github.com/itchyny/lightline.vim][lightline.vim]]
- [[https://github.com/neovim/neovim][Neovim]]
- [[https://github.com/nvim-lualine/lualine.nvim][lualine.nvim]]

* Illustration
#+html: <p align="center"><img src="img/timu-caribbean-vim.png" width="100%"/></p>

* Installation
** With [[https://github.com/junegunn/vim-plug][vim-plug]]

#+begin_src vimrc
  Plug 'https://gitlab.com/aimebertrand/timu-caribbean-vim.git'
#+end_src

** With [[https://github.com/folke/lazy.nvim][lazy.vim]]

#+begin_src lua
  {
    'https://gitlab.com/aimebertrand/timu-caribbean-vim.git',
  }
#+end_src

* Usage
** Vim
*** Colorscheme

#+begin_src vimrc
  colorscheme timu-caribbean-vim
#+end_src

*** lightline.vim

#+begin_src vimrc
  let g:lightline = {
      \ 'colorscheme': 'timu_caribbean_vim',
      \ }
#+end_src

** Neovim
*** Colorscheme

#+begin_src lua
  vim.cmd([[colorscheme timu-caribbean-vim]])
#+end_src

*** lualine.nvim

#+begin_src lua
  require('lualine').setup {
        options = { theme = 'timu_caribbean_dark' },
      }
#+end_src

* License
MIT license.
