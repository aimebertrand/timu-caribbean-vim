" Theme: Timu-Caribbean Vim Theme
" Copyright (c) 2023 Aimé Bertrand <aime.bertrand@macowners.club>
" License: MIT
" Color theme with cyan as a dominant color.
" Code boilerplate from nord-vim
" License: MIT

let s:timu_caribbean_vim_version="1.0.0"
let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:sg0 = ["#161a1f", "NONE"]
let s:sg1 = ["#222222", 0]
let s:sg2 = ["#333333", "NONE"]
let s:sg3 = ["#394851", 8]
let s:sg4 = ["#e8e9eb", "NONE"]
let s:sg5 = ["#f0f4fc", 7]
let s:sg6 = ["#ffffff", 15]
let s:sg7 = ["#7fffd4", 14]
let s:sg8 = ["#88c0d0", 6]
let s:sg9 = ["#87cefa", 4]
let s:sg10 = ["#59a5fe", 12]
let s:sg11 = ["#ff6c60", 1]
let s:sg12 = ["#7fffd4", 11]
let s:sg13 = ["#eedc82", 3]
let s:sg14 = ["#15bb84", 2]
let s:sg15 = ["#fa87ce", 5]

let s:p.normal.left = [ [ s:sg7, s:sg1 ], [ s:sg5, s:sg2 ] ]
let s:p.normal.middle = [ [ s:sg5, s:sg1 ] ]
let s:p.normal.right = [ [ s:sg5, s:sg2 ], [ s:sg5, s:sg2 ] ]
let s:p.normal.warning = [ [ s:sg1, s:sg13 ] ]
let s:p.normal.error = [ [ s:sg1, s:sg11 ] ]

let s:p.inactive.left =  [ [ s:sg1, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.inactive.middle = g:sg_uniform_status_lines == 0 ? [ [ s:sg5, s:sg1 ] ] : [ [ s:sg5, s:sg3 ] ]
let s:p.inactive.right = [ [ s:sg5, s:sg1 ], [ s:sg5, s:sg1 ] ]

let s:p.insert.left = [ [ s:sg11, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.replace.left = [ [ s:sg15, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.visual.left = [ [ s:sg13, s:sg1 ], [ s:sg5, s:sg1 ] ]

let s:p.tabline.left = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.middle = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.right = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.tabsel = [ [ s:sg1, s:sg8 ] ]

let g:lightline#colorscheme#timu_caribbean_vim#palette = lightline#colorscheme#flatten(s:p)
